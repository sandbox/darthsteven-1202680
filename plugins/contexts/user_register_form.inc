<?php

/**
 * @file
 *
 * Plugin to provide a user_register_form context
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("User register form"),
  'description' => t('A user register form.'),
  'context' => 'ctools_context_create_user_register_form',
  //'settings form' => 'ctools_context_user_register_form_settings_form',
  'keyword' => 'user_register',
  'context name' => 'user_register_form',
  //'convert list' => array('type' => t('Node type')),
  //'convert' => 'ctools_context_node_add_form_convert',
  //'placeholder form' => array(
  //  '#type' => 'textfield',
  //  '#description' => t('Enter the node type this context.'),
  //),
);

/**
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function ctools_context_create_user_register_form($empty, $data = NULL, $conf = FALSE) {
  static $created;
  $context = new ctools_context(array('form'));
  $context->plugin = 'user_register_form';

  if ($empty || (isset($created) && $created)) {
    return $context;
  }
  $created = TRUE;

  ctools_include('form');
  $form_id = 'user_register';

  $form_state = array('want form' => TRUE, 'args' => array());

  $file = drupal_get_path('module', 'user') . '/user.pages.inc';
  include_once './' . $file;
  // This piece of information can let other modules know that more files
  // need to be included if this form is loaded from cache:
  $form_state['form_load_files'] = array($file);
  
  // The user login form expects to be logged out.
  session_save_session(FALSE);
  $current_user = $GLOBALS['user'];
  $GLOBALS['user'] = drupal_anonymous_user();

  $form = ctools_build_form($form_id, $form_state);
  
  // Restore the old user.
  $GLOBALS['user'] = $current_user;
  session_save_session(TRUE);
  
  // In a form, $data is the object being edited.
  //$context->data     = $type;
  //$context->title    = $types[$type]->name;
  //$context->argument = $type;

  // These are specific pieces of data to this form.
  // All forms should place the form here.
  $context->form       = $form;
  $context->form_id    = 'user_register';
  $context->form_title = t('User register');
  return $context;
}
