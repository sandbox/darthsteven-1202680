<?php

/**
 * Specialized implementation of hook_page_manager_task_tasks(). See api-task.html for
 * more information.
 */
function page_manager_user_account_integration_user_login_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',
    'title' => t('User login template'),
    'admin title' => t('User login template'),
    'admin description' => t('When enabled, this overrides the default Drupal behavior for displaying the login screen at <em>user</em>. If you add variants, you may use selection criteria such. If no variant is selected, the default Drupal login screen will be used.'),
    'admin path' => 'user',

    // Callback to add items to the page managertask administration form:
    'task admin' => 'page_manager_user_account_integration_user_login_task_admin',

    'hook menu' => 'page_manager_user_account_integration_user_login_menu',
    'hook menu alter' => 'page_manager_user_account_integration_user_login_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context', // handler type -- misnamed
    'get arguments' => 'page_manager_user_account_integration_user_login_get_arguments',
    'get context placeholders' => 'page_manager_user_account_integration_user_login_get_contexts',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('page_manager_user_account_integration_user_login_disabled', TRUE),
    'enable callback' => 'page_manager_user_account_integration_user_login_enable',
  );
}

/**
 * Callback defined by page_manager_user_account_integration_user_login_page_manager_tasks().
 *
 * Alter the user view input so that user view comes to us rather than the
 * normal user view process.
 */
function page_manager_user_account_integration_user_login_menu_alter(&$items, $task) {
  if (variable_get('page_manager_user_account_integration_user_login_disabled', TRUE)) {
    return;
  }

  // Override the user view handler for our purpose.
  if ($items['user']['page callback'] == 'user_page' || variable_get('page_manager_override_anyway', FALSE)) {
    $items['user']['page callback'] = 'page_manager_user_account_integration_user_login';
    $items['user']['file path'] = $task['path'];
    $items['user']['file'] = $task['file'];
  }
  else {
    // automatically disable this task if it cannot be enabled.
    variable_set('page_manager_user_account_integration_user_login_disabled', TRUE);
    if (!empty($GLOBALS['page_manager_user_account_integration_enabling_user_login'])) {
      drupal_set_message(t('Page manager module is unable to enable user because some other module already has overridden with %callback.', array('%callback' => $items['user']['page callback'])), 'error');
    }
  }
}

/**
 * Entry point for our overridden user view.
 *
 * This function asks its assigned handlers who, if anyone, would like
 * to run with it. If no one does, it passes through to Drupal core's
 * user view, which is user_page_view().
 */
function page_manager_user_account_integration_user_login() {
  global $user;
  // If we're logged in, pass through to Drupal's normal behavior.
  if ($user->uid) {
    module_load_include('inc', 'user', 'user.pages');
    return user_page();
  }
  
  // Load my task plugin:
  $task = page_manager_get_task('user_login');

  // Load the account into a context.
  ctools_include('context');
  ctools_include('context-task-handler');
  $contexts = ctools_context_handler_get_task_contexts($task, '', array());

  $output = ctools_context_handler_render($task, '', $contexts, array());
  if ($output === FALSE) {
    // Fall back!
    module_load_include('inc', 'user', 'user.pages');
    $output = user_page();
  }
  return $output;
}

/**
 * Callback to get arguments provided by this task handler.
 *
 * Since this is the node view and there is no UI on the arguments, we
 * create dummy arguments that contain the needed data.
 */
function page_manager_user_account_integration_user_login_get_arguments($task, $subtask_id) {
  return array(
  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function page_manager_user_account_integration_user_login_get_contexts($task, $subtask_id) {
  return ctools_context_get_placeholders_from_argument(page_manager_user_account_integration_user_login_get_arguments($task, $subtask_id));
}

/**
 * Callback to enable/disable the page from the UI.
 */
function page_manager_user_account_integration_user_login_enable($cache, $status) {
  variable_set('page_manager_user_account_integration_user_login_disabled', $status);

  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['page_manager_user_account_integration_enabling_user_login'] = TRUE;
  }
}
