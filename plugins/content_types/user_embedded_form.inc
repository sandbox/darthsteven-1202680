<?php

/**
 * Callback function to supply a list of content types.
 */
function page_manager_user_account_integration_user_embedded_form_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Embed user form'),
    'icon' => drupal_get_path('module', 'ctools') . '/plugins/content_types/form/icon_form.png',
    'category' => 'Form',
    'defaults' => array(
      'override_title' => FALSE,
      'override_title_text' => '',
      'user_embedded_form_id' => '',
    ),
  );
}

function page_manager_user_account_integration_user_embedded_form_content_type_render($subtype, $conf, $panel_args, $context) {
  $block->module = 'page_manager_user_account_integration';
  $block->delta = $conf['user_embedded_form_id'];
  $block->subject = '';
  $block->content = '';
  
  if (!empty($conf['user_embedded_form_id'])) {
    if (module_exists('ctools')) {
      ctools_include('form');
      //Pass things down to the formbuilder
      $form_state = array(
        'user_embedded_form' => array (
          'panel_args' => $panel_args,
          'panel_context' => $context,
        ),
      );
      $block->content = ctools_build_form($conf['user_embedded_form_id'], $form_state);
    }
    else {
      $block->content = drupal_get_form($conf['user_embedded_form_id']);
    }
  }

  return $block;
}

/**
 * Edit form for the mission content type.
 */
function page_manager_user_account_integration_user_embedded_form_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];

  // Need to choose which form to embed here
  $form['user_embedded_form_id'] = array (
    '#type' => 'textfield',
    '#title' => t('Form id of form to embed'),
    '#description' => t('In most cases this will be the same as the name of the constructor function for the form.'),
    '#default_value' => $conf['user_embedded_form_id'],
    '#required' => TRUE,
  );

  return $form;
}

function page_manager_user_account_integration_user_embedded_form_content_type_edit_form_submit(&$form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}


/**
 * Returns the administrative title for a type.
 */
function page_manager_user_account_integration_user_embedded_form_content_type_admin_title($subtype, $conf, $context) {
  return t('Embedded form: @form_id', array('@form_id' => $conf['user_embedded_form_id']));
}
